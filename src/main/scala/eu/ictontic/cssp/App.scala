
package eu.ictontic.cssp

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.{linalg => sla}
import org.apache.spark.mllib.linalg.{Matrix, Matrices, Vectors, DenseMatrix, DenseVector}
import org.apache.spark.mllib.linalg.distributed.{RowMatrix, MatrixEntry, CoordinateMatrix}
import breeze.{linalg => bla}
import breeze.numerics._
import breeze.linalg.qrp.QRP
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.text.SimpleDateFormat;
import java.util.Date;

//import scala.collection.JavaConversions._

/**
  * @author Bruno Ordozgoiti
  */
object App {


  //TODO not working
  //Set current.date property for the logger to store log files in the
  //corresponding folders
  val dateFormat = new SimpleDateFormat("MMM-dd-yyyy-HH-mm");
  System.setProperty("current.date", dateFormat.format(new Date));

  val mandatoryArgs = List("k", "file")
  val optionalArgs = List(
    ("mp", "2"), 
    ("scaling", "PR"), 
    ("sampling", "OWN"), 
    ("sv", "100"),
    ("samples", "40"),
    ("tol", "1e-2"),
    ("it", "0"),
    ("svd", "FALSE"), 
    ("std", "TRUE")
  ).toMap
 
  def main(args : Array[String]) {

    val argPairs = optionalArgs ++ args.map(x => {
      val p = (x.tail tail) split "="      
      (p(0), p(1))
    }).toMap

    //Check that arguments are correct. Otherwise exit
    if ( mandatoryArgs.filter(argPairs.map(_._1).toList.contains(_)).length < mandatoryArgs.length || 
      !(argPairs.map(_._1).toList.filter(x => !(mandatoryArgs.contains(x) || optionalArgs.map(_._1).toList.contains(x))).isEmpty) ){
      Helper.printUsage
      return
    }

    val logger = LoggerFactory.getLogger("csspLogger")
    logger.trace("########################################")
    logger.trace("ARGS")
    argPairs.map(x => logger.trace(x._1 + " = " + x._2))
    logger.trace("########################################")

    val file = argPairs("file")
    val K = argPairs("k").toInt
    val minPartitions = argPairs("mp").toInt
    val SCALING = argPairs("scaling")
    val SAMPLING = argPairs("sampling")
    val SV_ENTRY = argPairs("sv").toInt
    val SAMPLES_ENTRY = argPairs("samples").toInt
    val TOL = argPairs("tol").toDouble
    val ITERATIONS = argPairs("it").toInt
    val DO_SVD = argPairs("svd")
    val DO_STD = argPairs("std")


    val conf = new SparkConf().setAppName("Twostage_scala")
    val sc = new SparkContext(conf)

    logger.trace("**************************************************");
    logger.trace("PICS: Parallelized Independent Column Selection");
    logger.trace("||||| => |||");
    logger.trace(":D");
    logger.trace("Starting job. Configuration:");
    for(t <- sc.getConf.getAll){
      logger.trace(t._1 + ": " + t._2);
    }
    logger.trace("**************************************************");


    //*********************************
    // Data input and preprocessing
    //*********************************
    val readStart = System.currentTimeMillis
    val distFile = sc.textFile(file, minPartitions)
    val readEnd = System.currentTimeMillis
    logger.trace("Input read time: " + (readEnd-readStart)/1000.0)

    val prerows = distFile.map(s => {
      val row = s.split(" ")
      Vectors.dense(row.map(_.toDouble))
    }).persist(StorageLevel.MEMORY_ONLY)
  
    val M = prerows.count
    val N = prerows.first.size
    val FACTOR = math.log(K);
    val C = math.min(math.max(2, K*FACTOR), N).toInt    
    val SAMPLES = SAMPLES_ENTRY match{
      case -1 => math.min((math.pow(2, math.sqrt(0.3*N-K))*2), 40).toInt
      case _ => SAMPLES_ENTRY
    }
    val SV = SV_ENTRY match{
      case -1 => N
      case _ => SV_ENTRY
    }

    logger.trace("M: " + M)
    logger.trace("N: " + N)
    logger.trace("K: " + K)
    logger.trace("C: " + C)
    logger.trace("Random samples: " + SAMPLES)

    val stdStart = System.currentTimeMillis
    
    val rows = DO_STD match {
      case "TRUE" => {
        val colAvgs = prerows.map(x => new bla.DenseVector(x.toArray)).reduce((a,b) => a + b).map(_ / M)
        val bcColAvgs = sc.broadcast(colAvgs)
        val colSds = prerows.map(x => {
          val avgs = bcColAvgs.value
          val v = new bla.DenseVector(x.toArray) - avgs
          (v :* v)
        }).reduce((a,b) => a + b).map(x => if (x == 0.0) 1 else math.sqrt(x/(M-1)))
        val bcColSds = sc.broadcast(colSds)
        
        prerows.map(x => {
          val sds = bcColSds.value
          val avgs = bcColAvgs.value
          val v = (new bla.DenseVector(x.toArray) - avgs) :/ sds
          //new org.apache.spark.mllib.linalg.DenseVector(v.toArray)
          Vectors.dense(v.toArray)
        }).persist(StorageLevel.MEMORY_ONLY)
      }
      case "FALSE" => prerows
    }
    val stdEnd = System.currentTimeMillis
    logger.trace("Standardization time: " + (stdEnd-stdStart)/1000.0)

    if (DO_STD == "FALSE"){
      prerows.unpersist(false)
    }


    //*********************************
    //*** The algorithm starts here ***
    val start = System.currentTimeMillis

    val sVectors = SAMPLING match{
      case "TRAD" => K
      case "OWN" => math.min(N, SV)
    }

    //Get the principal components    
    val svd = computePPCA(rows, sVectors, ITERATIONS, TOL, sc)
    val singularVectors = svd._1
    val singularValues = svd._2

    //Test numerical rank with the specified tolerance
    val compRank = singularValues.toArray.filter(_ > 1e-2).length
    logger.trace("Numerical rank at least " + compRank)

    //Sampling probabilities
    val amV = new bla.DenseMatrix(N toInt, sVectors toInt, singularVectors.toArray)
    val mV = SAMPLING match{
      case "TRAD" => amV(::, Range(0, K))
      case "OWN" => amV * bla.diag(new bla.DenseVector(singularValues.toArray))
    }
    val probabilities = Range(0,N).map(i => math.pow(bla.norm(mV(i, ::).t),2)).map(x => x/K.toDouble)
    val normProbs = probabilities.map(x => x/probabilities.sum)
    logger.trace("Probabilities")
    logger.trace("" + normProbs)
    val colSamples = this.randomlySample(Range(0,N).toList, normProbs.toList, C, SAMPLES)
    val vT = singularVectors.transpose
    
    //For each candidate subset, build the corresponding column sample
    //of Sigma V^T
    val chosenColumnLists = colSamples.map(s => {
      val sampledArray = s.map(x => Range(0,K).map(y => vT.apply(y,x) * (SCALING match {
        case "BOUT" => 1/(math.sqrt(math.min(1,C*probabilities(x))))
        case "PR" => math.pow(singularValues(y), 1)
        case "NONE" => 1
      }))).flatten.toArray
      val dm = new bla.DenseMatrix(K, s.length, sampledArray)
      //Run the RRQR and keep the first k columns
      val QRP(_QQ, _RR, _P, _pvt) = bla.qrp(dm)
      _pvt.take(K).map(x => s(x))
    })

    val choices = chosenColumnLists.filter(_.length >= K)
    val allChosenColumns = choices.map(_ toSet).reduce((x,y) => x union y).toList.sorted

    //Norm minimization phase: Calculate Qt A
    val bcAllChosen = sc.broadcast(allChosenColumns)
    val qtA = rows.map(row => {
      val allChosen = bcAllChosen.value
      val qRow = row.toArray.zipWithIndex.filter(allChosen contains _._2).map(_._1)
      val v1 = new bla.DenseMatrix(qRow.size, 1, qRow.toArray)
      val v2 = new bla.DenseMatrix(1, row.size, row.toArray)
      v1 * v2
    }).reduce((x,y) => x + y)
    logger.trace("Size of Q: " + qtA.rows + ", " + qtA.cols)
    
    //Extract the multiple instances of Ct C from Qt A by eliminating columns and
    //rows
    //approximators' structure: ((List of chosen columns, CpA), index)
    val approximators = choices.map(x =>{
      //Take the indices of the columns of Q that are in this C
      val colsInC = allChosenColumns.zipWithIndex.filter(x contains _._1).map(_._2)
      val ctc = qtA(colsInC.sorted, x.toList.sorted).toDenseMatrix
      val r = bla.rank(ctc)
      (x, ctc, r)
    }).filter(x => x._3 == K).map(x => {
      val ctc = x._2
      val colsInC = allChosenColumns.zipWithIndex.filter(x._1 contains _._1).map(_._2)
      val ctci = bla.inv(ctc)
      val ctA = qtA(colsInC, ::).toDenseMatrix
      List((x._1, ctci * ctA))
    }).reduce((x,y) => x ::: y).zipWithIndex

    //Finally, calculate the residual norm for all choices of C
    val bcApprox = sc.broadcast(approximators)
    val residualNorms = rows.map(row => {
      val aRow = new bla.DenseVector(row.toArray)
      val approxs = bcApprox.value
      //diffs' structure: (column choice index, norm of difference
      //between row of A and row of approximated A). List of tuples. 
      val diffs = approxs.map(x => {
        val cRow = aRow.toArray.zipWithIndex.filter(y => x._1._1 contains y._2).map(y => y._1)
        val cRowM = new bla.DenseMatrix(1, cRow.size, cRow.toArray)
        val cpa = x._1._2
        val approx = cRowM * cpa
        val d = (new bla.DenseVector(approx.toArray) - aRow)
        (x._2, (d :* d).sum)
      })
      diffs
    }).reduce((a,b) => ((a ::: b).groupBy(_._1).map(kv => (kv._1, kv._2.map(_._2).sum))).toList )

    val end = System.currentTimeMillis
    logger.trace("Algorithm time: " + (end-start)/1000.0)

    val winner = residualNorms.minBy(_._2)._1
    val minimumNorm = math sqrt (residualNorms.minBy(_._2)._2)
    logger.trace("Chosen subset: " + choices(winner).toList)
    logger.trace("Residual frobenius norm: " + minimumNorm)

    //Rebuild the matrix with SVD to obtain the lower bound to the
    //residual norm
    if(DO_SVD == "TRUE"){
      val svdStart = System.currentTimeMillis
      val dataPairs = rows.zipWithIndex.map(x => (x._2, x._1.toDense))
      val rmData = new RowMatrix(rows)
      val svdF = rmData.computeSVD(K, true, 1e-9)
      val sigmaVT = new DenseMatrix(svdF.s.size, N.toInt, svdF.V.transpose.toArray.zipWithIndex.map(x => x._1 * svdF.s((x._2 % svdF.s.size).toInt)))
      val aPrime = svdF.U.multiply(sigmaVT)
      val apPairs = aPrime.rows.zipWithIndex.map(x => (x._2, x._1.toDense))//rows.map(x => (x.index, x.vector.toDense))
      val apDataPairs = apPairs.join(dataPairs).values
      val residualNormSVD = math.sqrt(
        apDataPairs.map(rows => {
          val cRow = new bla.DenseVector(rows._1.toArray)
          val aRow = new bla.DenseVector(rows._2.toArray)
          val d = (cRow - aRow)
            (d :* d).sum
        }).reduce((x,y) => x+y)
      )
      val svdEnd = System.currentTimeMillis
      logger.trace("SVD residual time: " + (svdEnd-svdStart)/1000.0)
      logger.trace("Residual frobenius norm with SVD: " + residualNormSVD)
    }

   sc.stop

  }

  def randomlySample(l : List[Int], d : List[Double], c : Int, samples : Int) : List[Array[Int]] = {    
    var choices = new Array[Array[Int]](samples)
    for (i <- 0 to samples-1){
      var s = Set[Int]()
      var choice = new Array[Int](c)
      for (j <- 0 to c-1) {
        val distMap = (l zip d).filter(x => !(s contains x._1)).sortBy(_._2).reverse
        val top = distMap.map(_._2).reduce((a,b) => a+b)
        val vals = distMap.map(x => x._2)
        val indexedVals = vals.zipWithIndex
        val acum = indexedVals.map(x => (distMap(x._2)._1, (vals take x._2).sum))
        
        val r = scala.util.Random.nextDouble * top
        
        choice(j) = acum.filter(x => x._2 <= r).last._1
        s = s union Set(choice(j))
      }
      choices(i) = choice.distinct
    }
    return choices.toList

  }

  def computePPCA(normalizedRows : RDD[sla.Vector], sVectors : Int, ITERATIONS : Int, TOL : Double, sc : SparkContext) : (sla.DenseMatrix, sla.DenseVector) = {
    val logger = LoggerFactory.getLogger("csspLogger")
    val breezeRows = normalizedRows.map(x => new bla.DenseVector(x.toArray))
    val M = normalizedRows.count
    val N = normalizedRows.first.size
    //var W = bla.DenseMatrix.eye[Double](N)
    //W = W(::, Range(0, sVectors))
    var W = bla.DenseMatrix.zeros[Double](N, sVectors)
    Range(0, sVectors).map(x => W(x,x) = 1.0)
    var eSigma  = 1.0
    var converged = false
    var iteration = 0
    while(!converged){
      iteration = iteration + 1
      val matrixM = W.t * W + bla.DenseMatrix.eye[Double](sVectors)*eSigma
      val bcM = sc.broadcast(matrixM)
      val preFactors = breezeRows.map(x => {
        val iM = bla.inv(bcM.value)
        val wtx = W.t * x
        val xn = iM * wtx
        val xxt = eSigma * iM + xn * xn.t
        (xn, xxt, x)
      })
      //preFactors.cache

      val factors = preFactors.map(x => ((x._3 * x._1.t), x._2)).reduce((a,b) => (a._1 + b._1, a._2 + b._2))
      val newW = factors._1 * bla.inv(factors._2)

      val sigma = preFactors.map(x => {
        val xn = x._1
        val xxt = x._2
        val t = x._3
        val norm = math.pow(bla.norm(t), 2)
        val midTerm = 2.0 * xn.t * newW.t * t
        val finalTerm = bla.trace(xxt * newW.t * newW)
        (norm - midTerm + finalTerm)
      }).reduce((a,b) => a+b) * (1.0/(M*N).toDouble)
      //preFactors.unpersist(false)
  
      W = newW

      val sa = sigma.toArray
      val newSigma = sa(0)
      if(ITERATIONS <= 0){
        val err = math.abs(newSigma - eSigma)
        logger.trace("Diff: " + err)
        if (err < TOL){
          logger.trace("EM Done!")
          converged = true
        }
      } else if (iteration >= ITERATIONS) {
        converged = true
      }
      eSigma = newSigma
    }

    val wtw = W.t * W
    val bla.svd.SVD(_uu, _ss, _vv) = bla.svd(wtw)
    val vecsE = W * _vv.t
    //val estimatedSVectors = bla.qr.justQ(vecsE)
    val estimatedSVectors = vecsE
    val estimatedSValues = bla.diag((_vv*wtw*_vv.t + eSigma)).map(math.sqrt(_))

    return (new sla.DenseMatrix(estimatedSVectors.rows, estimatedSVectors.cols, estimatedSVectors.toArray), new sla.DenseVector(estimatedSValues.toArray))
  }

}

object Helper {
  val logger = LoggerFactory.getLogger("csspLogger")
  def printUsage(){
    val s =     "\r\n\r\n" +
    "----------\r\n" +
    "PICS: Parallelized Independent Column Selection." +
    "\r\n\r\n" +
    "Usage: spark-submit <jar-file>.jar --file=<input file> --k=<number of chosen columns> [--tol=<float> --it=<Integer> --mp=<minpartitions> --scaling=BOUT|PR --sampling=TRAD|OWN --svd=TRUE|FALSE --std=TRUE|FALSE]" +
    "\r\n\r\n" +
    "--tol=<float>\n" + 
    "\t The tolerance for the convergence of the EM algorithm.\n" + 
    "--it=<Integer>\n" + 
    "\t The number of iterations for the EM algorithm (Overrides tol).\n" + 
    "--mp=<Integer>\n" + 
    "\t The value of minPartitions for the input file.\n" + 
    "--scaling=BOUT|PR\n" + 
    "\t The approach for scaling the input of the RRQR (default PR).\n" + 
    "--sampling=TRAD|OWN\n" + 
    "\t The approach for sampling candidate subsets (default OWN).\n" + 
    "--svd=TRUE|FALSE\n" +
    "\t If TRUE, compute the residual for the best rank-k approximation at the end of the algorithm (default FALSE).\n" + 
    "--std=TRUE|FALSE]\n" +
    "\t If TRUE, standardize the data to zero mean and unit variance (default TRUE).\n"

    println(s)
    logger.trace(s)
  }

  def toBreezeDenseMatrix(v : org.apache.spark.mllib.linalg.DenseMatrix) : bla.DenseMatrix[Double] = {
    return new bla.DenseMatrix[Double](v.numRows, v.numCols, v.toArray)
  }

}
