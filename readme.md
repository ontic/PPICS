# PPICS

## Description
Probabilistic Parallelized Independent Columns Selection (PPICS) is an experimental algorithm designed to address a shortcoming of the feature selection algorithms described so far. All three algorithms depend on the calculation of the singular value decomposition, which in the case of tall, skinny matrices depends quadratically on the number of features. PPICS computes the leverage scores based on Probabilistic Principal Component Analysis, which can be approximated using an Expectation-Maximization (EM) approach that iterates in O(mnk) time. We provide a parallelized implementation of the EM algorithm. The rest of the feature selection process is carried out as in PICS.

PPICS offers the possibility to apply linear methods for unsupervised feature selection to big data sets comprised of a large number of features. This situation is likely to arise in many network management scenarios. For instance, when a network service provider wants to estimate certain QoE parameters for their users, it might be interesting to correlate network traces and application data, combining the extracted features and potentially giving rise to complex data samples described in thousands of variables. Similar situations will probably take place in virtualized network infrastructure environments, where Internet service providers might find it helpful to correlate data across the stack -e.g. network packets and cloud infrastructure events-, or in IoT scenarios, where events might represent data coming from tens of thousands of sensors.

## Parameters
* k: the number of columns (features) to keep.
* Tolerance: The EM algorithm is iterative, and is judged to have converged when the difference of the result between successive iterations is small. This parameter establishes the tolerated difference.
* Iterations: Rather than choosing a tolerance for the EM algorithm, the user can set this parameter to enforce a fixed number of iterations. We have observed that just three iterations tend to produce good results in terms of the chosen feature subset.
* Singular vectors: Since the matrix factorization is done on SV^T , the loadings on vectors corresponding to small singular values vanish and do not have a significant impact on the outcome of the algorithm. This parameter determines the number of singular vectors and values that will be taken into account. If the numerical rank of the input matrix is known to be less than n, lowering this parameter can make the algorithm faster with a negligible loss in accuracy.
* Sampling strategy: the user can choose to use a traditional rank-k leverage score sampling strategy, rather than the one we propose. This tends to provide better candidates when the data is standardized to unit variance. Defaults to our proposed method.
* Scaling: How to scale the input matrix for the RRQR. We consider the method proposed in [boutsidis2009] and the multiplication of the rows of V transposed by S, as done in our PRANKS algorithm. The latter is the one chosen by default.
* Samples: The number of random candidates to sample. A number around 50 tends to be enough, although more can be taken safely.

## How to

### Platform
The algorithm has been tested on a cluster of 10 worker nodes and one master node running CentOS 6.4 and equipped with an Intel quad-core processor and 4 GB of RAM each. All machines were connected through an ethernet switch with a capacity of 100 Mbps on each link. The implementation was done using Scala 2.10.4 on Spark 1.4.1. The HDFS distributed file system implemented on Hadoop 2.6 was employed for data storage and access. 

### Input file format
The expected input file is an mxn numeric matrix. Each of the m lines represents a matrix row, and consists of a string of n whitespace-separated numbers. For instance:

```0.4835231 0.3474168 0.9217592 0.6012819 ...```

```0.2328477 0.5248122 0.3980504 0.9192953 ...```

```0.2334433 0.5256567 0.3988392 0.9194879 ...```

### Compile and run
The project uses Maven to compile & build. To download the dependencies and create the required jar file in the `target` directory, type the following command from the main directory of the project:

```
mvn package
```

This will generate a new directory called target containing an executable jar file named `ppics-1.0.jar`. This jar is actually a Spark executable with all the necessary dependencies.

To run the algorithm:

```spark-submit target/pranks-1.0.jar –-file=<input-file> --k=<integer> [--tol=<float> --it=<Integer> --mp=<minPartitions> --sv=<svectors> --svd=<TRUE|FALSE> --std=<TRUE|FALSE>]```

```input-file: The relative path to the input file.```

```k: the number of columns (features) to keep.```

```tol: The tolerance for the convergence of the EM algorithm.```

```it: The number of iterations for the EM algorithm (Overrides tol).` ``

```minPartitions: The minimum number of partitions for the input file, as specified by Spark. ```

```scaling: The approach for scaling the input of the RRQR (default PR).```

```sampling: The approach for sampling candidate subsets (default OWN).```

```sv: The number of singular vectors and values to consider. Default: k.```

```svd: whether or not to compute the SVD to compare the obtained residual Frobenius norm to that of the best rank-k approximation. If TRUE, said norm will be also written to the output. Default: FALSE.```

```std: whether or not to standardize the data to zero mean and unit variance. Default: TRUE.```


The value of minPartitions can have a dramatic impact on performance. We recommend 2 per available core.

## Examples
A toy data set is available in `example/toy_data`. It consists of a 10x10 matrix with three sets of numerically dependent columns (linear combinations with additive Gaussian noise). Below is an example of an execution keeping 3 columns, running 3 iterations of the EM algorithm, computing the SVD to obtain the best rank-k approximation residual norm, standardizing the data and requesting 4 partitions.

```spark-submit target/ppics-1.0.jar --file="data/toy_data" --k=3 --it=3 –svd=TRUE --mp=4```

## Results and output file format
The algorithm uses SLF4J for logging and output, and can therefore work with any user-specified log4j properties file. An example log4j.properties is provided, however, with the following configuration.
* All output is redirected to different files in a folder called logs, which will be created at the time of the execution in the directory from which the Spark job was invoked.
* All output above the WARN level is written to logs/err.out
* All other output above the DEBUG level is written to logs/info.out
* All other output above the TRACE level is written to logs/trace.out

The results of the algorithm are written to logs/trace.out. The contents of this file can be described as follows:
* A list of the input parameters
* A list of the Spark configuration variables
* Information on the input matrix dimensions and other parameters, execution modes and partial execution times.
* If the tolerance criterion is used for convergence, the delta for each iteration is shown.
* The list of leverage scores (Probabilities)
* The size of matrix Q^T A (the number of rows equals the cardinality of the union of the candidate subsets)
* The total execution time in seconds (Algorithm time: x).
* The selected feature subset (Chosen subset:).
* The Frobenius norm of the residual matrix (Residual frobenius norm).
* If applicable, the Frobenius norm of the residual matrix obtained with the best rank-k approximation (SVD).